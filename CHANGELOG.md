# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1]

### Added

- Initialized VueJS project.
- [`LICENSE`](LICENSE) is [CC0](LICENSE).
- [`CODE_OF_CONDUCT.md`](CODE_OF_CONDUCT.md) that came from [contributor covenant](https://www.contributor-covenant.org/).
- [`CONTRIBUTING.md`](CONTRIBUTING.md) for general purpose contribution.
- [`CHANGELOG.md`](CHANGELOG.md) that follows [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Changed

- [`README.md`](README.md) proper initial content for the project.

[0.0.1]: https://gitlab.com/jericomanapsa/ipfs-chat/-/tags/v0.0.1
