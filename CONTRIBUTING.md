# Contributing

Thank you for your interest in contributing to the project. Currently, everything is experimental. Code may change with or without warnings. Nevertheless, please feel free to discuss things by opening an issue.

## Code Quality

1. Ensure that the changes you've made follows the standards that the project is using.
2. Make tests and update existing ones if necessary, to ensure that the code is working.

## Pull Request Process

1. Once the code is finalized, merge all your local changes to your local develop branch.
2. Increase the minor version or patch version in compliance with [SemVer](https://semver.org/).
3. Submit the merge request and wait for reviews.

## Code of Conduct

Please read and understand the (code of conduct)[CODE_OF_CONDUCT.md].
