export default async ({ Vue }) => {
  Vue.filter("peerIdToShort", function(peerId) {
    const firstEightChars = peerId.substr(0, 8);
    const lastEightChars = peerId.substr(peerId.length - 8, 8);
    return `${firstEightChars}...${lastEightChars}`;
  });
};
