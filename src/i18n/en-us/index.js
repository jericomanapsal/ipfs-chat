// This is just an example,
// so you can safely delete all default props below

export default {
  title: "IPFS Chat",
  joinRoom: "Join Room",
  conversation: {
    me: "me",
    writeMessage: "Write your first message!",
    selectRoom: "Select a room to view conversation"
  },
  null: {
    rooms: {
      title: "No rooms",
      description: "Join a room below"
    },
    peers: {
      title: "No peers",
      description: "Select a room to view its peers"
    }
  },
  toast: {}
};
