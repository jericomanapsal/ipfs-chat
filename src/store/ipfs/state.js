export default function() {
  return {
    ipfs: null,
    info: null,
    selectedRoomName: null,
    selectedRoom: null,
    rooms: {},
    peers: null
  };
}
