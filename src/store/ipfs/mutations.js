export async function mutateIpfs(state, ipfs) {
  state.ipfs = ipfs;
}

export async function mutateInfo(state, info) {
  state.info = info;
}

export function mutateSelectedRoomName(state, roomName) {
  state.selectedRoomName = roomName;
}

export function mutateSelectedRoom(state, conversation) {
  state.selectedRoom = conversation;
}

export function mutateAddRoom(state, { roomName, conversation }) {
  state.rooms = {
    ...state.rooms,
    [roomName]: conversation
  };
}

export function mutateDeleteRoom(state, roomName) {
  delete state.rooms[roomName];
}

export function mutatePeers(state, peers) {
  if (state.peers) {
    state.peers.length = 0;
  }
  state.peers = peers || [];
}

export function mutateAddMessage(state, { roomName, message }) {
  state.rooms[roomName] = [...state.rooms[roomName], message];
  state.selectedRoom = state.rooms[roomName];
}
