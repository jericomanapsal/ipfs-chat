export async function initializeIpfs(context) {
  const ipfs = await window.ipfs.enable({
    commands: [
      "add",
      "cat",
      "get",
      "ls",
      "dag",
      "pin",
      "repo",
      "stats",
      "id",
      "swarm",
      "dht",
      "ping"
    ]
  });
  await context.commit("mutateIpfs", ipfs);
}

export async function retrieveId(context) {
  const info = await context.state.ipfs.id();
  await context.commit("mutateInfo", info);
}

export async function selectRoom(context, roomName) {
  const roomNames = Object.keys(context.state.rooms);
  if (!!roomName && roomNames.includes(roomName)) {
    const room = context.state.rooms[roomName];
    const roomPeers = await context.state.ipfs.pubsub.peers(roomName);
    await context.commit("mutateSelectedRoomName", roomName);
    await context.commit("mutateSelectedRoom", room);
    await context.commit("mutatePeers", roomPeers);
  } else {
    await context.commit("mutateSelectedRoomName", null);
    await context.commit("mutateSelectedRoom", null);
    await context.commit("mutatePeers", null);
  }
}

export async function addRoom(context, roomName) {
  const roomNames = Object.keys(context.state.rooms);
  if (!roomNames.includes(roomName)) {
    const conversation = [];
    await context.commit("mutateAddRoom", { roomName, conversation });
    await context.state.ipfs.pubsub.subscribe(roomName, async message => {
      await context.commit("mutateAddMessage", { roomName, message });
    });
  }
}

export async function deleteRoom(context, roomName) {
  const roomNames = Object.keys(context.state.rooms);
  if (roomNames.includes(roomName)) {
    await context.state.ipfs.pubsub.unsubscribe(roomName);
    await context.commit("mutateDeleteRoom", roomName);
  }
}

export async function sendMessage(context, message) {
  const currentRoom = context.state.selectedRoomName;
  await context.state.ipfs.pubsub.publish(currentRoom, message);
}
