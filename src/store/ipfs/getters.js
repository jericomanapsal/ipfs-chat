export function getRoomNames(state) {
  const roomNames = Object.keys(state.rooms);
  return roomNames;
}
