# node:lts
FROM node@sha256:c185973d2d8d60d2f99abe1285e1ad0887a42f313f3befa6ae8f2429304ee09c as builder
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN yarn install --frozen-lockfile
RUN yarn run build

# golang:1.14.0-alpine
FROM golang@sha256:7f85a7ea01c40560696d1d2809e0640a4bd775a6b219140a519484a79fb9ac2a
RUN apt update && apt install git ca-certificates && update-ca-certificates
RUN apt clean
ENV USER=appuser
ENV UID=10001
RUN adduser \
    --disabled-password \
    --gecos "" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    $USER
RUN mkdir /hash && mkdir /ipfs && mkdir /ipfs-chat
ENV IPFS_PATH /ipfs
COPY --from=builder /app/dist/spa /ipfs-chat
RUN GO111MODULE=on go get github.com/ipfs/ipfs-update
RUN /go/bin/ipfs-update install --no-check v0.4.23
RUN /go/bin/ipfs init -e -b 4096 -p server
RUN /go/bin/ipfs daemon --enable-pubsub-experiment &
RUN /go/bin/ipfs add -r -Q --pin /ipfs-chat > /hash/ipfs-chat
RUN chown -R appuser:appuser /ipfs
RUN chmod -R 755 /ipfs
USER appuser:appuser
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD curl http://127.0.0.1:8080/ipfs/$(cat /hash/ipfs-chat) || exit 1
ENTRYPOINT ["/go/bin/ipfs", "daemon", "--enable-pubsub-experiment"]
EXPOSE 8080
EXPOSE 4001
EXPOSE 5001
