# ipfs-chat

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![Code style prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![MIT license](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)

Master:
[![pipeline status](https://gitlab.com/jericomanapsal/ipfs-chat/badges/master/pipeline.svg)](https://gitlab.com/jericomanapsal/ipfs-chat/commits/master)
[![coverage report](https://gitlab.com/jericomanapsal/ipfs-chat/badges/master/coverage.svg)](https://gitlab.com/jericomanapsal/ipfs-chat/commits/master)

Develop:
[![pipeline status](https://gitlab.com/jericomanapsal/ipfs-chat/badges/develop/pipeline.svg)](https://gitlab.com/jericomanapsal/ipfs-chat/commits/develop)
[![coverage report](https://gitlab.com/jericomanapsal/ipfs-chat/badges/develop/coverage.svg)](https://gitlab.com/jericomanapsal/ipfs-chat/commits/develop)

Experimental chat application that is built on top of IPFS.

## Getting Started

This project is a simple chat application utilizing the IPFS platform. It uses pubsub as a communication layer.

## Development

It is highly recommended to use Mozilla Firefox or Brave as main web browser for development. IPFS Companion extension must also be installed.

### Dependency Setup

The project mainly uses `yarn` although `npm` or other package managers can still be used. It is highly recommended to use the local binaries in the project in development. To install, simply issue the corresponding command:

```bash
yarn install
```

or in `npm`

```bash
npm isntall
```

### Project Setup

Clone the repository using SSH or HTTPS:

```bash
git clone git@gitlab.com:jericomanapsal/ipfs-chat.git
```

or

```bash
https://gitlab.com/jericomanapsal/ipfs-chat.git
```

Then install the dependencies:

```
cd ipfs-chat
yarn install
```

### Linting

ESLint is used in this project together with Prettier. These are heavily enforced and will cause error when not followed. To run the linter for checking type in:

```bash
yarn run lint
```

or to automatically fix formatting

```bash
yarn run lint --fix
```

### Unit Testing

```bash
yarn run test:unit
```

Enable watch mode by appending `:watch` or `:watchAll`.

```bash
yarn run test:unit:watchAll
```

### E2E Testing

To be implemented.

### Audits

#### node_modules

```bash
yarn run audit:node_modules
```

#### licenses

```bash
yarn run audit:licenses
```

#### lighthouse

To be implemented.

#### snyk

To be implemented.

## Building

To build, make sure you are in the root path of the repository, then issue the command:

```bash
yarn run build
```

The output folder by default is located at `./dist/spa/`.

## Built With

- [VueJS](https://vuejs.org/) - JavaScript Framework
- [Quasar](https://quasar.dev/) - UI Framework
- [IPFS](https://ipfs.io/) - P2P Platform
- [IPFS Room](https://github.com/ipfs-shipyard/ipfs-pubsub-room) - Pubsub Layer
- [libsodium](https://download.libsodium.org/doc/) - Encryption Layer

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on code of conduct, and the process for contributing to the project.

## Code of Conduct

Please read, understand, and apply the [code of conduct](CODE_OF_CONDUCT.md) to maintain the atmosphere of collaboration.

## Versioning

[SemVer](http://semver.org/) is the standard used for versioning in this project.

## Authors

### Maintainer

[<img
    src="https://gitlab.com/uploads/-/system/user/avatar/2370349/avatar.png#author"
    alt="Jerico Manapsal Picture"
    style="width:100px !important;border-radius:50%"/>](https://gitlab.com/jericomanapsal)

[@jericomanapsal](https://gitlab.com/jericomanapsal)


## License

The tools, libraries, and other software used in the creation of this application uses varying licenses. Visit their repositories for more information.

For this work, it is licensed under CC0, essentially placing it under public domain.

[![cc0](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, [jericomanapsal](https://gitlab.com/jericomanapsal/ipfs-chat) has waived all copyright and related or neighboring rights to IPFS Chat.
