import { createLocalVue } from "@vue/test-utils";
import VueI18n from "vue-i18n";
import Vuex from "vuex";
import * as All from "quasar";
import messages from "@/i18n";

export const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

const _localVue = createLocalVue();
_localVue.use(Vuex);
_localVue.use(VueI18n);
_localVue.use(All.Quasar, {
  components,
  directives: All
});
export const localVue = _localVue;
export const i18n = new VueI18n({
  locale: "en-us",
  fallbackLocale: "en-us",
  messages
});
