import ipfs from "@/store/ipfs";

describe("store: ipfs", () => {
  describe("state", () => {
    const stateSnapshot = {
      ipfs: null,
      info: null,
      selectedRoomName: null,
      selectedRoom: null,
      rooms: {},
      peers: null
    };
    const state = ipfs.state();
    expect(state).toMatchSnapshot(stateSnapshot);
  });

  describe("mutations", () => {
    test("mutateIpfs", async () => {
      const state = { ipfs: null };
      const input = { obj: "sample ipfs object" };
      await ipfs.mutations.mutateIpfs(state, input);
      expect(state).toHaveProperty("ipfs", input);
    });

    test("mutateInfo", async () => {
      const state = { info: null };
      const input = { obj: "sample ipfs id output" };
      await ipfs.mutations.mutateInfo(state, input);
      expect(state).toHaveProperty("info", input);
    });

    test("mutateSelectedRoomName", async () => {
      const state = { selectedRoomName: null };
      const input = "ipfs-pubsub-test-100";
      await ipfs.mutations.mutateSelectedRoomName(state, input);
      expect(state).toHaveProperty("selectedRoomName", input);
    });

    test("mutateSelectedRoom", async () => {
      const state = { selectedRoom: null };
      const input = [{ data: "array of messages" }];
      await ipfs.mutations.mutateSelectedRoom(state, input);
      expect(state).toHaveProperty("selectedRoom", input);
    });

    test("mutateAddRoom", async () => {
      const state = { rooms: null };
      const roomName = "ipfs-pubsub-test-100";
      const conversation = [];
      await ipfs.mutations.mutateAddRoom(state, { roomName, conversation });
      expect(state.rooms).toHaveProperty(roomName, conversation);
    });

    test("mutateDeleteRoom", async () => {
      const state = { rooms: { "ipfs-pubsub-test-100": [] } };
      const input = "ipfs-pubsub-test-100";
      await ipfs.mutations.mutateDeleteRoom(state, input);
      expect(state.rooms).not.toHaveProperty(input);
    });

    test("mutatePeers: peers is null", async () => {
      const state = { peers: null };
      const input = ["peerId"];
      await ipfs.mutations.mutatePeers(state, input);
      expect(state).toHaveProperty("peers", input);
    });

    test("mutatePeers: peers is not null", async () => {
      const state = { peers: [] };
      const input = ["peerId"];
      await ipfs.mutations.mutatePeers(state, input);
      expect(state).toHaveProperty("peers", input);
    });

    test("mutatePeers: peers input is null", async () => {
      const state = { peers: [] };
      const input = null;
      await ipfs.mutations.mutatePeers(state, input);
      expect(state).toHaveProperty("peers", []);
    });

    test("mutateAddMessage", async () => {
      const state = {
        rooms: { "ipfs-pubsub-test-100": [] },
        selectedRoom: null
      };
      const input = {
        roomName: "ipfs-pubsub-test-100",
        message: "Hello World! :D"
      };
      await ipfs.mutations.mutateAddMessage(state, input);
      expect(state.rooms).toHaveProperty(input.roomName, [input.message]);
      expect(state.selectedRoom).toBe(state.rooms[input.roomName]);
    });
  });

  describe("getters", () => {
    test("getRoomNames", async () => {
      const state = { rooms: { "ipfs-pubsub-test-100": [] } };
      const result = await ipfs.getters.getRoomNames(state);
      expect(result).toStrictEqual(["ipfs-pubsub-test-100"]);
    });
  });

  describe("actions", () => {
    let windowSpy;

    beforeEach(() => {
      windowSpy = jest.spyOn(global, "window", "get");
    });

    afterEach(() => {
      windowSpy.mockRestore();
    });

    test("initializeIpfs", async () => {
      const enable = jest.fn();
      windowSpy.mockImplementation(() => ({
        ipfs: { enable }
      }));
      const context = { commit: jest.fn() };
      await ipfs.actions.initializeIpfs(context);
      expect(enable).toHaveBeenCalled();
      expect(context.commit).toHaveBeenCalledWith("mutateIpfs", undefined);
    });

    test("retrieveId", async () => {
      const id = jest.fn();
      const commit = jest.fn();
      const context = {
        state: { ipfs: { id } },
        commit
      };
      await ipfs.actions.retrieveId(context);
      expect(id).toHaveBeenCalled();
      expect(commit).toHaveBeenCalledWith("mutateInfo", undefined);
    });

    test("selectRoom: room name is valid", async () => {
      const roomName = "ipfs-pubsub-test-100";
      const peers = jest.fn();
      const commit = jest.fn();
      const context = {
        state: {
          ipfs: { pubsub: { peers } },
          rooms: { [roomName]: [] }
        },
        commit
      };
      await ipfs.actions.selectRoom(context, roomName);
      expect(peers).toHaveBeenCalled();
      expect(commit.mock.calls).toEqual([
        ["mutateSelectedRoomName", roomName],
        ["mutateSelectedRoom", []],
        ["mutatePeers", undefined]
      ]);
    });

    test("selectRoom: room name is invalid", async () => {
      const commit = jest.fn();
      const context = {
        state: { rooms: {} },
        commit
      };
      await ipfs.actions.selectRoom(context);
      expect(commit.mock.calls).toEqual([
        ["mutateSelectedRoomName", null],
        ["mutateSelectedRoom", null],
        ["mutatePeers", null]
      ]);
    });

    test("addRoom: includes room name", async () => {
      const roomName = "ipfs-pubsub-test-100";
      const commit = jest.fn();
      const subscribe = jest.fn();
      const context = {
        state: {
          ipfs: { pubsub: { subscribe } },
          rooms: { [roomName]: [] }
        },
        commit
      };
      await ipfs.actions.addRoom(context, roomName);
      expect(commit).not.toHaveBeenCalled();
      expect(subscribe).not.toHaveBeenCalled();
    });

    test("addRoom: does not include room name", async () => {
      const roomName = "ipfs-pubsub-test-100";
      const commit = jest.fn();
      const subscribe = jest.fn();
      const context = {
        state: {
          ipfs: { pubsub: { subscribe } },
          rooms: {}
        },
        commit
      };
      await ipfs.actions.addRoom(context, roomName);
      expect(commit).toHaveBeenCalledWith("mutateAddRoom", {
        roomName,
        conversation: []
      });
      expect(subscribe).toHaveBeenCalled();
    });

    test("addRoom: trigger callback when message arrives", async () => {
      const roomName = "ipfs-pubsub-test-100";
      const message = "hello new room";
      const commit = jest.fn();
      const subscribe = jest.fn();
      const context = {
        state: {
          ipfs: { pubsub: { subscribe } },
          rooms: {}
        },
        commit
      };
      await ipfs.actions.addRoom(context, roomName);
      const callback = subscribe.mock.calls[0][1];
      callback(message);
      const addMessageArgs = commit.mock.calls[1];
      expect(addMessageArgs[0]).toBe("mutateAddMessage");
      expect(addMessageArgs[1]).toStrictEqual({ roomName, message });
    });

    test("deleteRoom: room exists", async () => {
      const roomName = "ipfs-pubsub-test-100";
      const commit = jest.fn();
      const unsubscribe = jest.fn();
      const context = {
        state: {
          ipfs: { pubsub: { unsubscribe } },
          rooms: { [roomName]: [] }
        },
        commit
      };
      await ipfs.actions.deleteRoom(context, roomName);
      expect(unsubscribe).toHaveBeenCalledWith(roomName);
      expect(commit).toHaveBeenCalledWith("mutateDeleteRoom", roomName);
    });

    test("deleteRoom: room does not exists", async () => {
      const roomName = "ipfs-pubsub-test-100";
      const commit = jest.fn();
      const unsubscribe = jest.fn();
      const context = {
        state: {
          ipfs: { pubsub: { unsubscribe } },
          rooms: {}
        },
        commit
      };
      await ipfs.actions.deleteRoom(context, roomName);
      expect(unsubscribe).not.toHaveBeenCalled();
      expect(commit).not.toHaveBeenCalled();
    });

    test("sendMessage", async () => {
      const selectedRoomName = "ipfs-pubsub-test-100";
      const message = "Hello World! :D";
      const publish = jest.fn();
      const context = {
        state: {
          ipfs: { pubsub: { publish } },
          selectedRoomName
        }
      };
      await ipfs.actions.sendMessage(context, message);
      expect(publish).toHaveBeenCalledWith(selectedRoomName, message);
    });
  });
});
