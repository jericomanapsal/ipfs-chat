import { mount } from "@vue/test-utils";
import { localVue, i18n } from "../testutils";
import RoomsList from "@/components/RoomsList.vue";

describe("RoomsList.vue", () => {
  describe("mount", () => {
    test("mounts the component", () => {
      const wrapper = mount(RoomsList, { localVue, i18n });
      expect(wrapper.isVueInstance()).toBeTruthy();
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe("props: rooms", () => {
    test("rooms is null", () => {
      const wrapper = mount(RoomsList, {
        localVue,
        i18n,
        propsData: {
          rooms: null
        }
      });
      expect(wrapper.element).toMatchSnapshot();
    });

    test("rooms is empty array", () => {
      const wrapper = mount(RoomsList, {
        localVue,
        i18n,
        propsData: {
          rooms: []
        }
      });
      expect(wrapper.element).toMatchSnapshot();
    });

    test("rooms has string elements", () => {
      const wrapper = mount(RoomsList, {
        localVue,
        i18n,
        propsData: {
          rooms: [
            "ipfs-pubsub-test-100",
            "hello-world",
            "ipfs_is_awesome",
            "pubsub-is-the-transport"
          ]
        }
      });
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe("events: click", () => {
    test("room item is clicked", () => {
      const rooms = [
        "ipfs-pubsub-test-100",
        "hello-world",
        "ipfs_is_awesome",
        "pubsub-is-the-transport"
      ];
      const selectedRoomIndex = 2;
      const wrapper = mount(RoomsList, {
        localVue,
        i18n,
        propsData: { rooms }
      });
      const roomsDOM = wrapper.findAll("div.q-item__label.overflow-hidden");
      const selectedRoomDom = roomsDOM.at(selectedRoomIndex);
      expect(selectedRoomDom.text()).toBe(rooms[selectedRoomIndex]);
    });
  });
});
