import { mount } from "@vue/test-utils";
import { localVue, i18n } from "../testutils";
import PeersList from "@/components/PeersList.vue";

describe("PeersList.vue", () => {
  describe("mount", () => {
    test("mounts the component", () => {
      const wrapper = mount(PeersList, { localVue, i18n });
      expect(wrapper.isVueInstance()).toBeTruthy();
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe("props: peers", () => {
    test("peers is null", () => {
      const wrapper = mount(PeersList, {
        localVue,
        i18n,
        propsData: {
          peers: null
        }
      });
      expect(wrapper.element).toMatchSnapshot();
    });

    test("peers is empty array", () => {
      const wrapper = mount(PeersList, {
        localVue,
        i18n,
        propsData: {
          peers: []
        }
      });
      expect(wrapper.element).toMatchSnapshot();
    });

    test("peers has string elements", () => {
      const wrapper = mount(PeersList, {
        localVue,
        i18n,
        propsData: {
          peers: [
            "QmRnG6HXET11eWAhxciS78KqZABDkEPdQQy53bBRyNUatr",
            "QmeQK9yqE4EPyuQwdDU2j3fyLbJUFYF6eV5dxhWzRAbkK7",
            "QmU4hCHejD98j68qqqyhFZXdHqRixbSmYMhufEeR9ZF2Ng"
          ]
        }
      });
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe("events: click", () => {
    test("peer item is clicked", () => {
      const peers = [
        "QmRnG6HXET11eWAhxciS78KqZABDkEPdQQy53bBRyNUatr",
        "QmeQK9yqE4EPyuQwdDU2j3fyLbJUFYF6eV5dxhWzRAbkK7",
        "QmU4hCHejD98j68qqqyhFZXdHqRixbSmYMhufEeR9ZF2Ng"
      ];
      const selectedPeerIndex = 1;
      const wrapper = mount(PeersList, {
        localVue,
        i18n,
        propsData: { peers }
      });
      const peersDOM = wrapper.findAll("div.q-item__label.ellipsis");
      const selectedPeerDom = peersDOM.at(selectedPeerIndex);
      expect(selectedPeerDom.text()).toBe(peers[selectedPeerIndex]);
    });
  });
});
