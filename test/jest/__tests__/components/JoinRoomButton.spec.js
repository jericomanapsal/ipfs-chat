import { mount } from "@vue/test-utils";
import { localVue, i18n } from "../testutils";
import JoinRoomButton from "@/components/JoinRoomButton.vue";

describe("JoinRoomButton.vue", () => {
  describe("mount", () => {
    test("mounts the component", () => {
      const wrapper = mount(JoinRoomButton, { localVue, i18n });
      expect(wrapper.isVueInstance()).toBeTruthy();
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe("events: click", () => {
    test("clicks the button", () => {
      const wrapper = mount(JoinRoomButton, { localVue, i18n });
      wrapper.find("button").trigger("click");
      expect(wrapper.emitted().click).toBeTruthy();
      expect(wrapper.element).toMatchSnapshot();
    });
  });
});
