import { mount } from "@vue/test-utils";
import { localVue, i18n } from "../testutils";
import MessageInput from "@/components/MessageInput.vue";

describe("MessageInput.vue", () => {
  describe("mount", () => {
    test("mounts the component", async () => {
      const wrapper = mount(MessageInput, {
        localVue,
        i18n,
        propsData: {
          inputId: "bfa7f0dd-de35-4b1e-9f66-0f8704c070cb",
          buttonId: "04aa438b-ad14-480d-86a0-8c3fa60d3923"
        }
      });
      expect(wrapper.isVueInstance()).toBeTruthy();
    });
  });

  describe("props: enabled", () => {
    test("enables the text input and send button", () => {
      const wrapper = mount(MessageInput, {
        localVue,
        i18n,
        propsData: {
          enabled: true,
          inputId: "ed2877cb-bf9d-4101-96a1-d9b7893fd413",
          buttonId: "08718ccb-4861-467c-a196-f5f184a52f84"
        }
      });
      const textInput = wrapper.find("input[disabled]");
      const sendButton = wrapper.find("button[disabled]");
      expect(textInput.exists()).toBeFalsy();
      expect(sendButton.exists()).toBeFalsy();
    });

    test("disables the text input and send button", () => {
      const wrapper = mount(MessageInput, {
        localVue,
        i18n,
        propsData: {
          enabled: false,
          inputId: "00cc91d4-6ac3-4dd2-9bd1-0f05f5693061",
          buttonId: "5734408c-aae1-46a1-9130-401f76bccd55"
        }
      });
      const textInput = wrapper.find("input[disabled]");
      const sendButton = wrapper.find("button[disabled]");
      expect(textInput.exists()).toBeTruthy();
      expect(sendButton.exists()).toBeTruthy();
    });
  });

  describe("text input and send", () => {
    test("inputs a message then sends it", () => {
      const wrapper = mount(MessageInput, {
        localVue,
        i18n,
        propsData: {
          enabled: true,
          inputId: "b7b06121-5d20-4b01-8bda-b729bba14861",
          buttonId: "11467ca7-d23d-423c-bb30-09f64579543b"
        }
      });
      const message = "hello world!";
      const textInput = wrapper.find("input");
      const sendButton = wrapper.find("button");
      textInput.setValue(message);
      textInput.trigger("change");
      expect(wrapper.vm.message).toBe(message);
      sendButton.trigger("click");
      expect(wrapper.emitted().send[0][0]).toBe(message);
    });

    test("clicks send without typing a message", () => {
      const wrapper = mount(MessageInput, {
        localVue,
        i18n,
        propsData: {
          enabled: true,
          inputId: "df6036e9-1cc6-4fe4-8fbd-6c97fe5fc0bf",
          buttonId: "2b4999cb-5f07-4ddd-8f3c-3df8ad196bdc"
        }
      });
      const sendButton = wrapper.find("button");
      sendButton.trigger("click");
      expect(wrapper.emitted().send).toBeUndefined();
    });
  });
});
