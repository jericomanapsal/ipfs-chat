import { mount } from "@vue/test-utils";
import Vuex from "vuex";
import { localVue, i18n } from "../testutils";
import ConversationList from "@/components/ConversationList.vue";

describe("ConversationList.vue", () => {
  describe("mount", () => {
    test("mounts the component", () => {
      const wrapper = mount(ConversationList, { localVue, i18n });
      expect(wrapper.isVueInstance()).toBeTruthy();
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe("props: conversation", () => {
    test("conversation is null", () => {
      const wrapper = mount(ConversationList, {
        localVue,
        i18n,
        propsData: {
          conversation: null
        }
      });
      expect(wrapper.element).toMatchSnapshot();
    });

    test("conversation is empty array", () => {
      const wrapper = mount(ConversationList, {
        localVue,
        i18n,
        propsData: {
          conversation: []
        }
      });
      expect(wrapper.element).toMatchSnapshot();
    });

    test("conversation has messages", () => {
      const conversation = [
        {
          from: "QmRnG6HXET11eWAhxciS78KqZABDkEPdQQy53bBRyNUatr",
          data: Buffer.from("hello world", "utf8")
        },
        {
          from: "QmeQK9yqE4EPyuQwdDU2j3fyLbJUFYF6eV5dxhWzRAbkK7",
          data: Buffer.from("hi there", "utf8")
        },
        {
          from: "QmU4hCHejD98j68qqqyhFZXdHqRixbSmYMhufEeR9ZF2Ng",
          data: Buffer.from("cool", "utf8")
        }
      ];
      const sender = conversation[0].from;
      const store = new Vuex.Store({
        state: { ipfs: { info: { id: sender } } }
      });
      const wrapper = mount(ConversationList, {
        localVue,
        i18n,
        store,
        propsData: { conversation }
      });
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe("autoscroll when new message ", () => {
    test("scrolls when new message is added", async () => {
      const setScrollPosition = jest.fn();
      const qScrollAreaStub = {
        render: () => {},
        methods: { setScrollPosition }
      };
      const conversation = [
        {
          from: "QmRnG6HXET11eWAhxciS78KqZABDkEPdQQy53bBRyNUatr",
          data: Buffer.from("hello world", "utf8")
        },
        {
          from: "QmeQK9yqE4EPyuQwdDU2j3fyLbJUFYF6eV5dxhWzRAbkK7",
          data: Buffer.from("hi there", "utf8")
        },
        {
          from: "QmU4hCHejD98j68qqqyhFZXdHqRixbSmYMhufEeR9ZF2Ng",
          data: Buffer.from("cool", "utf8")
        }
      ];
      const newConversation = [
        ...conversation,
        {
          from: "QmRnG6HXET11eWAhxciS78KqZABDkEPdQQy53bBRyNUatr",
          data: Buffer.from("new Message", "utf8")
        }
      ];
      const sender = conversation[0].from;
      const store = new Vuex.Store({
        state: { ipfs: { info: { id: sender } } }
      });
      const wrapper = mount(ConversationList, {
        localVue,
        i18n,
        store,
        propsData: { conversation },
        stubs: {
          "q-scroll-area": qScrollAreaStub
        }
      });
      wrapper.setProps({ conversation: newConversation });
      await wrapper.vm.$nextTick();
      expect(setScrollPosition).toHaveBeenCalled();
      expect(wrapper.element).toMatchSnapshot();
    });

    test("ignores scrolling when there are no messages", async () => {
      const setScrollPosition = jest.fn();
      const qScrollAreaStub = {
        render: () => {},
        methods: { setScrollPosition }
      };
      const conversation = [
        {
          from: "QmRnG6HXET11eWAhxciS78KqZABDkEPdQQy53bBRyNUatr",
          data: Buffer.from("hello world", "utf8")
        },
        {
          from: "QmeQK9yqE4EPyuQwdDU2j3fyLbJUFYF6eV5dxhWzRAbkK7",
          data: Buffer.from("hi there", "utf8")
        },
        {
          from: "QmU4hCHejD98j68qqqyhFZXdHqRixbSmYMhufEeR9ZF2Ng",
          data: Buffer.from("cool", "utf8")
        }
      ];
      const sender = conversation[0].from;
      const store = new Vuex.Store({
        state: { ipfs: { info: { id: sender } } }
      });
      const wrapper = mount(ConversationList, {
        localVue,
        i18n,
        store,
        propsData: { conversation },
        stubs: {
          "q-scroll-area": qScrollAreaStub
        }
      });
      wrapper.setProps({ conversation: [] });
      await wrapper.vm.$nextTick();
      expect(setScrollPosition).not.toHaveBeenCalled();
      expect(wrapper.element).toMatchSnapshot();
    });
  });
});
