import { mount, shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import { localVue, i18n } from "../testutils";
import MainLayout from "@/layouts/MainLayout.vue";

describe("MainLayout.vue", () => {
  describe("mount", () => {
    test("mounts the component", () => {
      const store = new Vuex.Store({
        state: { ipfs: {} },
        getters: { getRoomNames: () => [] }
      });
      const wrapper = shallowMount(MainLayout, { localVue, i18n, store });
      expect(wrapper.isVueInstance()).toBeTruthy();
    });
  });

  describe("computed", () => {
    test("title: default value", () => {
      const defaultValue = "IPFS Chat";
      const store = new Vuex.Store({
        state: { ipfs: { selectedRoomName: null } }
      });
      const wrapper = mount(MainLayout, { localVue, i18n, store });
      const vm = wrapper.vm;
      expect(vm.title).toBe(defaultValue);
    });

    test("title: custom value", () => {
      const selectedRoomName = "ipfs-pubsub-test-100";
      const store = new Vuex.Store({
        state: { ipfs: { selectedRoomName } }
      });
      const wrapper = mount(MainLayout, { localVue, i18n, store });
      const vm = wrapper.vm;
      expect(vm.title).toBe(selectedRoomName);
    });

    test("rooms", () => {
      const roomName = "ipfs-pubsub-test-100";
      const store = new Vuex.Store({
        modules: {
          ipfs: {
            state: { ipfs: {} },
            getters: { getRoomNames: () => [roomName] }
          }
        }
      });
      const wrapper = mount(MainLayout, { localVue, i18n, store });
      const vm = wrapper.vm;
      expect(vm.$store.getters["getRoomNames"]).toStrictEqual([roomName]);
    });

    test("peers", () => {
      const peers = [];
      const store = new Vuex.Store({
        state: { ipfs: { peers } }
      });
      const wrapper = mount(MainLayout, { localVue, i18n, store });
      const vm = wrapper.vm;
      expect(vm.peers).toBe(peers);
    });
  });

  describe("methods", () => {
    test("promptRoomName: dialog display", async () => {
      const onOk = jest.fn();
      const store = new Vuex.Store({
        state: { ipfs: {} }
      });
      const wrapper = shallowMount(MainLayout, {
        localVue,
        i18n,
        store,
        mocks: {
          $q: { dialog: () => ({ onOk }) }
        }
      });
      await wrapper.vm.promptRoomName();
      expect(onOk).toHaveBeenCalled();
    });

    test("promptRoomName: joins room after dialog display", async () => {
      const onOk = jest.fn();
      const joinRoom = jest.fn();
      const roomName = "ipfs-pubsub-test-100";
      const store = new Vuex.Store({
        state: { ipfs: {} }
      });
      const wrapper = shallowMount(MainLayout, {
        localVue,
        i18n,
        store,
        methods: { joinRoom },
        mocks: {
          $q: { dialog: () => ({ onOk }) }
        }
      });
      await wrapper.vm.promptRoomName();
      const onOkCallback = onOk.mock.calls[0][0];
      onOkCallback(roomName);
      expect(joinRoom).toHaveBeenCalledWith(roomName);
    });

    test("joinRoom: room is invalid", async () => {
      const notify = jest.fn();
      const roomName = null;
      const store = new Vuex.Store({
        state: { ipfs: {} }
      });
      const wrapper = shallowMount(MainLayout, {
        localVue,
        i18n,
        store,
        mocks: {
          $q: { notify }
        }
      });
      const vm = wrapper.vm;
      await vm.joinRoom(roomName);
      expect(notify).toHaveBeenCalled();
    });

    test("joinRoom: room is valid", async () => {
      const mockAddRoom = jest.fn();
      const then = jest.fn();
      const roomName = "ipfs-pubsub-test-100";
      const store = new Vuex.Store({
        state: { ipfs: {} }
      });
      const wrapper = shallowMount(MainLayout, {
        localVue,
        i18n,
        store,
        methods: {
          addRoom: roomName => {
            mockAddRoom(roomName);
            return { then };
          }
        },
        mocks: {
          $q: { notify: () => {} }
        }
      });
      await wrapper.vm.joinRoom(roomName);
      expect(mockAddRoom).toHaveBeenCalledWith(roomName);
      expect(then).toHaveBeenCalled();
    });

    test("joinRoom: room successfully joined", async () => {
      const notify = jest.fn();
      const selectRoom = jest.fn();
      const then = jest.fn();
      const roomName = "ipfs-pubsub-test-100";
      const store = new Vuex.Store({
        state: { ipfs: {} }
      });
      const wrapper = shallowMount(MainLayout, {
        localVue,
        i18n,
        store,
        methods: {
          addRoom: () => ({ then }),
          selectRoom
        },
        mocks: {
          $q: { notify }
        }
      });
      await wrapper.vm.joinRoom(roomName);
      const resolveCallback = then.mock.calls[0][0];
      resolveCallback();
      expect(selectRoom).toHaveBeenCalledWith(roomName);
      expect(notify).toHaveBeenCalled();
    });

    test("joinRoom: failed to join room", async () => {
      const notify = jest.fn();
      const then = jest.fn();
      const roomName = "ipfs-pubsub-test-100";
      const store = new Vuex.Store({
        state: { ipfs: {} }
      });
      const wrapper = shallowMount(MainLayout, {
        localVue,
        i18n,
        store,
        methods: {
          addRoom: () => ({ then })
        },
        mocks: {
          $q: { notify }
        }
      });
      await wrapper.vm.joinRoom(roomName);
      const rejectCallback = then.mock.calls[0][1];
      rejectCallback();
      expect(notify).toHaveBeenCalled();
    });

    test("peerClick", async () => {
      const dialog = jest.fn();
      const peerId = "QmU4hCHejD98j68qqqyhFZXdHqRixbSmYMhufEeR9ZF2Ng";
      const store = new Vuex.Store({
        state: { ipfs: {} }
      });
      const wrapper = shallowMount(MainLayout, {
        localVue,
        i18n,
        store,
        mocks: {
          $q: { dialog }
        }
      });
      await wrapper.vm.peerClick(peerId);
      expect(dialog).toHaveBeenCalledWith({
        title: "Peer ID",
        message: peerId
      });
    });
  });
});
