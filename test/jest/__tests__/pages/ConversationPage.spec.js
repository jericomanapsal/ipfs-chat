import { shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import { localVue } from "../testutils";
import ConversationPage from "@/pages/ConversationPage.vue";

describe("ConversationPage.vue", () => {
  describe("mount", () => {
    test("mounts the component", () => {
      const store = new Vuex.Store({
        state: { ipfs: { selectedRoom: "ipfs-pubsub-test-100" } }
      });
      const wrapper = shallowMount(ConversationPage, {
        localVue,
        store
      });
      expect(wrapper.isVueInstance()).toBeTruthy();
      expect(wrapper.element).toMatchSnapshot();
    });
  });
});
