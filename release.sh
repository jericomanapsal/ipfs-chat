#!/bin/bash

yarn run clean
yarn run build
cd ./dist
mv spa ipfs-chat
zip -r -Z deflate ipfs-chat.zip ipfs-chat/
tar cvf ipfs-chat.tar ipfs-chat/
tar cvzf ipfs-chat.tar.gz ipfs-chat/
tar cvjf ipfs-chat.tar.bz2 ipfs-chat/
gpg -o ipfs-chat.zip.sig -b ipfs-chat.zip
gpg -o ipfs-chat.tar.sig -b ipfs-chat.tar
gpg -o ipfs-chat.tar.gz.sig -b ipfs-chat.tar.gz
gpg -o ipfs-chat.tar.bz2.sig -b ipfs-chat.tar.bz2
sha256sum ipfs-chat.* > digest
rm -rf ipfs-chat
